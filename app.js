const express = require('express');
const {MqttClient} = require('./modules/mqtt');

const app = express();
const PORT = process.env.PORT || 5015;

app.use(express.json());

let client = new MqttClient('cloud');

app.get('/ping', (req, res) => {
    res.send(200)
});

app.get('/setCloud', (req, res) => {
    client.close();
    setTimeout(() => {
        client = null;
        client = new MqttClient('cloud');
        res.send(200);
    }, 13000);
    
});

app.get('/setLocal', (req, res) => {
    client.close();
    setTimeout(() => {
        client = null;
        client = new MqttClient('local');
        res.send(200);
    }, 13000);
    
});

app.listen(PORT, () => {
    console.log(`Example app listening at http://localhost:${PORT}`);
});
