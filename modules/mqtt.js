const mqtt = require('mqtt');
const {Topic} = require('./topic');
const writeLog = require('./logger.js');
const fs = require('fs')

class MqttClient {
    constructor(enviroment) {

        try {
            const data = fs.readFileSync(`config-${enviroment}.json`, 'utf8')
            const config = JSON.parse(data.toString());
            console.log(config.mqtt.options.host);
            this.initialize(config);
        } catch (err) {
            console.error(err)
        }

    }

    initialize(config) {
        this.client  = mqtt.connect(config.mqtt.options);
        const currentTopics = [];

        config.topics.forEach(item => {
            const topicToAdd = new Topic(item.topicName, item.protocol, item.target, item.ip, item.port, this.client);
            currentTopics.push(topicToAdd);
            this.client.subscribe(topicToAdd.name);
        });

        this.client.on('connect', function () {
            console.log('MQTT connected');
        });

        this.client.on('error', function (error) {
            console.error(`MQTT client error: ${error}`);
        });

        this.client.on('close', function() {
            console.log('MQTT connection closed.');
        })

        setTimeout(() => {
            this.client.on('message', function (topic, message) {
                console.log(message.toString())
                const handler = currentTopics.find(t => t.name === topic);
                if(handler){
                    const msg = JSON.parse(message.toString());
                    handler.do(msg, topic);
                }else{
                    writeLog({name: topic}, 'topic doesn`t found');
                }
                
            });
        }, 6000);
        
    }

    close() {
        this.client.end();
    }
}

module.exports = {
    MqttClient
};