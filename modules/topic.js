const Telnet = require('telnet-client');
const writeLog = require('./logger');
const cp = require('child_process');
const fetch = require('node-fetch');
const net = require('net')
const md5 = require('md5')

class Topic {
    constructor(name, protocol, target, ip, port, client) {
        // console.log(ip);
        this.name = name;
        this.protocol = protocol;
        this.target = target;
        this.ip = ip;
        this.port = port;
        this.client = client;
        this.initialState = true;
        this.winInterval = null;

        this.client.on('close', () => {
            console.log('Close Interval');
            clearInterval(this.winInterval);
        })

        this.init();
    }

    async init() {
        if(this.protocol === 'Telnet'){
            if(this.name.includes('CONTROL')){
                this.client.publish(this.name.replace('CONTROL', 'STATUS'), JSON.stringify({state: 'active' }), {retain: true});
            }
        }else if(this.protocol === 'Windows'){
            if(this.name.includes('CONTROL')){
                this.client.publish(this.name.replace('CONTROL', 'STATUS'), JSON.stringify({state: 'active' }), {retain: true});
            }
        }
    }

    do(command, topic){ 
        
        if(this.protocol === 'Windows'){

            console.log(command);
            this.doWindows(command);
            try{
                if(topic.includes('CONTROL')){
                    const status = topic.replace('CONTROL', 'STATUS');
                    this.client.publish(status , JSON.stringify({ state: command.state }), {retain: true})
                }
            }catch(e){
                console.log(e);
            }
            
        }else if(this.protocol === 'BrightSign-CEC'){

            console.log('BrightSign-CEC');
            this.doBrightSignCEC(command, topic);

        }else if(this.protocol === 'BrightSign-Volume'){

            console.log('BrightSign-Volume');
            this.doBrightSignVolume(command, topic);

        }else if(this.protocol === 'Telnet'){

            this.doTelnet(command, topic);

        }
        
    }

    doCOM(portNum, command) {
        const port = new SerialPort(portNum);
        const parser = port.pipe(new Readline({ delimiter: '\r\n' }));
        
        // Open errors will be emitted as an error event
        port.on('error', function(err) {
            console.log('Error: ', err.message);
            writeLog(err, ' COM port');
        });

        // Switches the port into "flowing mode"
        parser.on('data', function (data) {
            console.log('Data:', data)
        });
        
        port.write(`${command}`, function(err) {
            if (err) {
                return console.log('Error on write: ', err.message)
            }
            console.log('message written')
        });

    }

    doTelnet(command, topic) {
        let index = 0;
        let reqResults = true;
        for (const addr of this.ip) {
            index += 1;
            try{
                if(this.target === 'Panasonic'){
                    try {
               
                        const socket = new net.Socket()
            
                        socket.on('error', (err) => {
                            console.log(err)
                        })
            
                        socket.on('data', (data) => {
                            // console.log(data.toString())
                            if(data.toString().includes('00ER')){
                                reqResults = false;
                            }
                            const response = data.toString().split(' ');
                            if(response.length > 2){

                                var escaped = response[2].replace('\r','')
                            
                                if(command.state === "standby" || command.state === "poweroff"){
                                    const command = md5('dispadmin:admin:' + escaped) + '00POF\r'
                                    // console.log('dispadmin:admin:' + escaped)
                    
                                    // console.log(command)
                                    socket.write(command)
                                }else if(command.state === 'active'){
                                    const command = md5('dispadmin:admin:' + escaped) + '00PON\r'
                                    // console.log('dispadmin:admin:' + escaped)
                    
                                    // console.log(command)
                                    socket.write(command)
                                }

                            }
        
                        })
            
                        socket.connect(this.port, addr, () => {
                            // Get Auth data
                        })
            
                    } catch(error) {
                        // handle the throw (timeout)
                        console.log(error);
                        writeLog(error, ' Telnet connetion error.');
                    }
                }else if(this.target === 'BenQ'){

                    setTimeout(() => { 
                        let projectorCommand = '';
        
                        if(command.state === 'standby' || command.state === 'poweroff'){
                            projectorCommand = '<CR>*pow=off#<CR>';
                        }else if(command.state === 'active'){
                            projectorCommand = '<CR>*pow=on#<CR>';
                        }
                        
                        const socketClient = new net.Socket();
                        socketClient.connect(this.port, addr, () => {
                            // console.log('CONNECTED TO: ' + addr + ':' + this.port);
                            // console.log(projectorCommand);
                            socketClient.write(projectorCommand);
                        });
                        socketClient.on('data', function(data) { // 'data' is an event handler for the client socket, what the server sent
                            console.log('DATA: ' + data.toString());
                            socketClient.destroy(); // Close the client socket completely
        
                        });
                        // Add a 'close' event handler for the client socket
                        socketClient.on('close', function() {
                            console.log('Connection closed');
                        });
                        socketClient.on('error', (err) => {
                            console.log(err);
                        });

                    }, 11000 * index);

                }else if(this.target === 'LGTV'){

                    setTimeout(() => {
                        let projectorCommand = '';
    
                        if(command.state === 'standby' || command.state === 'poweroff'){
                            projectorCommand = 'kd 00 01\r';
                        }else if(command.state === 'active'){
                            projectorCommand = 'kd 00 00\r';
                        }
        
                        var client = new net.Socket();
                        client.connect(this.port, addr, () => {
                            console.log('CONNECTED TO: ' + addr + ':' + this.port);
                            client.write(projectorCommand);
                        });
        
                        client.on('data', function(data) { // 'data' is an event handler for the client socket, what the server sent
                            console.log('DATA: ' + data.toString());
                            client.destroy(); // Close the client socket completely
                        });
        
                        // Add a 'close' event handler for the client socket
                        client.on('close', function() {
                            console.log('Connection closed');
                        });
                    }, 2000 * index)
                    
                }else if(this.target === 'Viewsonic'){
                    var bytesToOn = [0x06, 0x14, 0x00, 0x04, 0x00, 0x34, 0x11, 0x00, 0x00, 0x5D],
                        hexValOn = new Uint8Array(bytesToOn)

                    var bytesToOff = [0x06, 0x14, 0x00, 0x04, 0x00, 0x34, 0x11, 0x01, 0x00, 0x5E],
                        hexValOff = new Uint8Array(bytesToOff)

                    var bytesToCheck = [0x07, 0x14, 0x00, 0x05, 0x00, 0x34, 0x00, 0x00, 0x11,0x00,0x5E],
                        hexValCheck = new Uint8Array(bytesToCheck)

                    let commandToExecute = '';
                    if(command.state === "standby" || command.state === "poweroff"){
                        commandToExecute = hexValOff;
                    }else if(command.state === 'active'){
                        commandToExecute = hexValOn;
                    }

                    try{
                        const client = new net.Socket();
                        client.connect(this.port, addr, () => {
                            console.log('CONNECTED TO: ' + addr + ':' + this.port);
                            client.write(commandToExecute);

                            setTimeout(() => {
                                client.destroy();
                            }, 5000);
                        });

                        client.on('data', function(data) { // 'data' is an event handler for the client socket, what the server sent
                            console.log('DATA: ' + data.toString());
                            client.destroy(); // Close the client socket completely
                        });

                        // Add a 'close' event handler for the client socket
                        client.on('close', function() {
                            console.log('Connection closed');
                        });
                        client.on('error', (err) => {
                            reqResults = false;
                        });
                    }catch(e){
                        console.log(e)
                    }
                    
                        
                }
            }catch(e){}
            
        }

        if(this.target === 'LGTV'){
            const status = topic.replace('CONTROL', 'STATUS');
            this.client.publish(status , JSON.stringify({ state: command.state }), {retain: true})
        }else if(this.target === 'BenQ'){
            const status = topic.replace('CONTROL', 'STATUS');
            this.client.publish(status , JSON.stringify({ state: command.state }), {retain: true})
        }else if(this.target === 'Panasonic'){
            if(reqResults){
                try{
                    this.client.publish(topic.replace('CONTROL', 'STATUS'), JSON.stringify(command), {retain: true})
                }catch(e){
                    console.log(e);
                }
            }
        }else if(this.target === 'Viewsonic'){
            if(reqResults){
                try{
                    this.client.publish(topic.replace('CONTROL', 'STATUS'), JSON.stringify(command), {retain: true})
                }catch(e){
                    console.log(e);
                }
            }
        }
    
    }

    doWindows(command) {
        if(command.hasOwnProperty('state')){
            if(command.state === 'standby'){

                winStandby();
                this.winInterval = setInterval(() => {
                    console.log('Interval');
                    winStandby();
                }, 20000);
                
                // const touchScreenBat = require.resolve(`${process.cwd()}/touchScreenDisable.bat`);
                // const touchScreenLs = cp.spawn(touchScreenBat, []);
                
            }else if(command.state === 'active'){
                clearInterval(this.winInterval);
                const bat = require.resolve(`${process.cwd()}/turnon.bat`);
                const ls = cp.spawn(bat, []);
                const sndBat = require.resolve(`${process.cwd()}/soundOn.bat`);
                const sndLs = cp.spawn(sndBat, []);
                // const touchScreenBat = require.resolve(`${process.cwd()}/touchScreenEnable.bat`);
                // const touchScreenLs = cp.spawn(touchScreenBat, []);
            }else if(command.state === 'poweroff'){
                winPowerOff();
            }
        }else if(command.hasOwnProperty('mute')){
            if(command.mute === true){
                const bat = require.resolve(`${process.cwd()}/soundOff.bat`);
                const ls = cp.spawn(bat, []);
                
            }else if(command.mute === false){
                const bat = require.resolve(`${process.cwd()}/soundOn.bat`);
                const ls = cp.spawn(bat, []);
                
            }
            if(command.hasOwnProperty('volume')){
                if(command.volume){
                    const bat = require.resolve(`${process.cwd()}/volume.bat`);
                    const volume =  command.volume;
                    
                    console.log(volume);
                    const delta = (65500 / 100) * (Number(volume) * 100);
        
                    const child = cp.spawn(bat, ['--volume', delta]);
        
                }
            }
        }else if(command.hasOwnProperty('volume')){
            if(command.volume){
                const bat = require.resolve(`${process.cwd()}/volume.bat`);
                const volume =  command.volume;
                
                console.log(volume);
                const delta = (65500 / 100) * (Number(volume) * 100);
    
                const child = cp.spawn(bat, ['--volume', delta]);
    
            }
        }
        
    }

    async doCEC(command) {
        // 
        let url = `http://${this.ip}:${this.port}/${command}`;

        let settings = { 
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({})
        };

        try {
            let result = await fetch(url, settings)
            .then(res => {
            });
        } catch(e) {
            console.log(e);
        }
        // 
    }

    doBrightSignCEC(command, topic) {
        console.log('BS STANDBY TOPIC');
        if(command.state === "standby" || command.state === "poweroff"){
            console.log(this.ip)
            for (const addr of this.ip) {
                try {
                    fetch(`http://${addr}:${this.port}/monitor-off`)
                        .then(response => {
                            // console.log(response);
                        });
                    fetch(`http://${addr}:${this.port}/mute`)
                        .then(response => {
                            // console.log(response);
                        });
                } catch (error) {
                    console.log(error);
                }
            }
           
            this.client.publish(topic.replace('CONTROL', 'STATUS'), JSON.stringify({state: command.state }), {retain: true})
        }else if(command.state === 'active'){
            for (const addr of this.ip) {
                try {
                    fetch(`http://${addr}:${this.port}/monitor-on`)
                        .then(response => {
                            // console.log(response);
                        });
                    fetch(`http://${addr}:${this.port}/unmute`)
                        .then(response => {
                            // console.log(response);
                        });
                } catch (error) {
                    console.log(error);
                }
            }
            
            this.client.publish(topic.replace('CONTROL', 'STATUS'), JSON.stringify({state: command.state }), {retain: true})
        }
    }

    doBrightSignVolume(command, topic) {
        console.log('BS VOLUME TOPIC');
        console.log(command);
        if(!this.ip){
            return;
        }
        if(command.hasOwnProperty('mute')){
            if(command.mute === true){
                for (const addr of this.ip) {
                    try {
                        fetch(`http://${addr}:${this.port}/mute`)
                            .then(response => {
                                // console.log(response);
                            });
                    } catch (error) {
                        console.log(error);
                    }
                }
               
                // this.client.publish(topic.replace('VOLUME', 'VOLUME'), JSON.stringify({ command }), {retain: true})
            }else if(command.mute === false){
                for (const addr of this.ip) {
                    try {
                        fetch(`http://${addr}:${this.port}/unmute`)
                            .then(response => {
                                // console.log(response);
                            });
                    } catch (error) {
                        console.log(error);
                    }
                }
                
                // this.client.publish(topic.replace('VOLUME', 'VOLUME'), JSON.stringify({ command }), {retain: true})
            }

            if(command.hasOwnProperty('volume')){
                for (const addr of this.ip) {
                    try {
                        fetch(`http://${addr}:${this.port}/volume-level?level=${command.volume}`)
                            .then(response => {
                                // console.log(response);
                            });
                    } catch (error) {
                        console.log(error);
                    }
                }
            }

        }else if(command.hasOwnProperty('volume')){
            for (const addr of this.ip) {
                try {
                    fetch(`http://${addr}:${this.port}/volume-level?level=${command.volume}`)
                        .then(response => {
                            // console.log(response);
                        });
                } catch (error) {
                    console.log(error);
                }
            }
        }
        
    }

}

function winStandby() {
    const bat = require.resolve(`${process.cwd()}/turnoff.bat`);
    const ls = cp.spawn(bat, []);
    
    ls.stdout.on("data", (data) => {
        console.log(`stdout: ${data}`);
        writeLog(data, '')
    });
    ls.stderr.on("data", (data) => {
        console.error(`stderr: ${data}`);
        writeLog(data, '')
    });
    ls.on("close", (code) => {
        console.log(`child process exited with code ${code}`);
        writeLog(code, '')
    });

    const sndBat = require.resolve(`${process.cwd()}/soundOff.bat`);
    const sndLs = cp.spawn(sndBat, []);
}

function winPowerOff() {
    const bat = require.resolve(`${process.cwd()}/poweroff.bat`);
    const ls = cp.spawn(bat, []);
    
    ls.stdout.on("data", (data) => {
        console.log(`stdout: ${data}`);
        writeLog(data, '')
    });
    ls.stderr.on("data", (data) => {
        console.error(`stderr: ${data}`);
        writeLog(data, '')
    });
    ls.on("close", (code) => {
        console.log(`child process exited with code ${code}`);
        writeLog(code, '')
    });
}

module.exports.Topic = Topic;