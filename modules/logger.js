const fs = require('fs');

/**
  * Запись сообщения в лог-файл.
  * @param  {Object} e   Объект ошибки.
  * @param  {String} type   Строка, характеризующая тип записи.
  */
function writeLog(e, type, client)  {
  let date_ob = new Date();
  let hours = date_ob.getHours();
  let minutes = date_ob.getMinutes() < 10 ? '0' + date_ob.getMinutes() : date_ob.getMinutes();
  let day = date_ob.getDate() < 10 ? '0' + date_ob.getDate() : date_ob.getDate();
  let month = (date_ob.getMonth() + 1) < 10 ? '0' + (date_ob.getMonth() + 1) : date_ob.getMonth() + 1;

  ensureDir('log');
  fs.appendFile('log/log.txt', `${day}.${month} --- ${hours}:${minutes} ---- ${type}: | ${e}\n`, (err) => {});
}

/**
  * Проверка существования директории на файловой системе, ее создание в случае отсутствия данной папки.
  * @param  {String} dir    Путь директории на файловой системе.
  */
function ensureDir(dir) {
  if (!fs.existsSync(dir)){
    fs.mkdirSync(dir);
  }
}

module.exports = writeLog;